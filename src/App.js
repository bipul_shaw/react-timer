import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Timer from './Timer';
import Button from './Button';
class App extends React.Component {
  state = { active: { t1: false, t2: false } };
  clicked = (id) => {
    this.setState(prevState => ({
      active: {
        ...prevState.active,
        [id]: !prevState.active[id]
      }
    }));
  }
  check = () => {
    if (this.state.active.t1 === true && this.state.active.t2 === true) {
      return true;
    }
    else {
      return false;
    }
  }
  update = (r) => {
    if (r) {
      this.setState({
        active: {
          t1: false,
          t2: false
        }
      })
    }
    else {
      this.setState({
        active: {
          t1: true,
          t2: true
        }
      })
    }
   
  }
  render() {
    let a;
    a = this.check();
    return (
      <div>
        <Timer st={this.state.active.t1} tag={1} />
        <Button flag={this.state.active.t1} handleClick={() => this.clicked("t1")} />
        <Timer st={this.state.active.t2} tag={2} />
        <Button flag={this.state.active.t2} handleClick={() => this.clicked("t2")} />
        <br></br>
        <br></br>
        <br></br>
        <Button flag={a} handleClick={() => this.update(a)} />
      </div>
    );
  }
}

export default App;