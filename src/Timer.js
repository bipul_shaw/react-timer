import React from 'react';
class Timer extends React.Component {
  state = { min: "00", sec: "00" };
  componentDidMount() {
    if (this.state.status) {
      this.timerID = setInterval(() => this.tick(), 1000);
    }
  }
  componentDidUpdate(prevProps) {
    if (prevProps.st !== this.props.st) {
      if (this.props.st) {
        this.timerID = setInterval(() => this.tick(), 1000);
      }
      else {
        clearInterval(this.timerID);
      }
    }
  }
  componentWillUnmount() {
    clearInterval(this.timerID);
  }
  tick = () =>{
    let s1=parseInt(this.state.sec), s2, s3, m1=parseInt(this.state.min), m2, m3;
    s2=(s1+1)%60;
    if(s2<10)
     s3=0+String(s2);
    else 
     s3=String(s2);
    m2=m1+Math.floor((s1+1)/60)
    if(m2<10)
     m3=0+String(m2);
    else
     m3=String(m2); 
    this.setState({
      sec: s3,
      min: m3
    });}
  render() {
    return (
      <div>
        <h1>{this.state.min}:{this.state.sec}</h1>
        <h2>Timer {this.props.tag}</h2>
      </div>
    );
  }
}

export default Timer;